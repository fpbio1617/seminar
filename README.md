# Seminar zum Fachprojekt Bioinformatik WS 2016/17 #

Jede Gruppe bekommt ein eigenes Unterverzeichnis für ihre Materialien.
Jedes Unterverzeichnis sollte eine README-Datei enthalten, die eine Hilfe zur Nutzung des Materials enthält.

Die folgenden Unterverzeichnisse sollten erstellt werden:

* 1-numpy-scipy
* 2-pandas
* 3-matplotlib-seaborn
* 4-altair-spyre
* 5-bokeh
* 6-numba
