from collections import Counter

# einfaches Skript
print("FP Bio")

dna = "AAAGGTTCCAGT"  # oder: input() fuer interaktive Eingabe
print("DNA-Sequenz: {}; Länge {}".format(dna, len(dna)))
r1 = dna[::-1]
r2 = "".join(reversed(dna))

r3 = ""
for c in dna:
	r3 = c + r3

r4 = []
for i in range(len(dna)-1, -1, -1):
	r4.append(dna[i])
r4 = "".join(r4)

print("rückwärts: {} oder {} oder {} oder {}".format(r1, r2, r3, r4))

c = Counter(dna)
x = c['G'] / len(dna)  if len(dna)>0  else 0.0
# x = len(dna)>0? expression : 0.0
print("Anteil G: {:.1%}".format(x))

