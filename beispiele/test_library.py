from library import square

def test_square_1():
    assert square(0) == 0

def test_square_2():
    assert square(-1) == square(1)

def test_square_3():
    assert square(13) == 169

def test_square_4():
    for i in range(20):
        assert square(i) == i**2
