"""
beispiel2.py

bytes vs. strings (ASCII vs. unicode)
string translation: str.maketrans, str.translate
dictionaries
Das random-Modul (random, choice, sample)
Dokumentation: docstrings
Generatoren, itertools (z.B. enumerate)
Kontextmanager: with-Statement
Datei-Operationen: with open(filename, "rt") as f
"""

s = "üß"  # str
b = s.encode()  # bytes
s1 = b.decode()  # wieder str, sollte gleich s sein
print(type(s), type(b), type(s1), s==s1)

# noch eine M
transtable = {'A': 'T', 'T':'A', 'G':'C', 'C':'G'}
transtable = dict(A='T', C='G', G='C', T='A')  # dasselbe
dna = 'GATTACA'
rc = "".join([transtable[c] for c in dna][::-1])

import random
L = [random.random() for _ in range(10)]  # Liste von 10 Zufallszahlen in [0,1[
print(L)
element = random.choice("ACGT")  # zufaelliges Element aus dem Container
print(element)
sample = random.sample("12345678", 3) # 3 zufaellige verschiedene! Elemente
print(sample)

def fib():
	"""yields every Fibonacci number in sorted order"""
	a = b = 1
	yield a
	yield b
	while True:
		a, b = a+b, a
		yield a

for i, n in enumerate(fib()):
	print(i, n)
	if n >= 2**10:  # auch mal 2**100 versuchen!
		break

# siehe auch modul itertools, z.B. itertools.product, etc.


# Arbeiten mit Dateien
# Alle Zeilen in dieser Datei, die mit 'def ' anfangen, ausdrucken
# (das sollte nur 'def fib():' sein!)
with open("beispiel2.py", "rt") as inputfile:
	# mode "rt": read/text -- also "rb": read binary, "wt": write text, ...
	for line in inputfile:
		if line.startswith("def "):
			print(line, end="") # Zeilenumbruch \n schon in line enthalten
print("--ENDE--")

