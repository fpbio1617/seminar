# Aufruf

```
python numba_edit_distance.py
```

Parameter (Stringlänge, Anzahl Berechnungen, Alphabetgröße) müssen direkt im Quellcode in der main() Funktion geändert werden.

Ein Kommandozeileninterface wäre eine nette Bereicherung.


# Umgebung

Der Aufruf muss in einem conda environment mit numpy und numba geschehen.
