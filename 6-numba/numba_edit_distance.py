# numba example: edit distance

import numpy as np
from numpy.random import randint
from numba import njit, int64


@njit(locals=dict(m=int64, n=int64))
# locals is optional, can help with type inference
def edit_distance(s, t):
    """return edit distance between sequences s and t"""
    m = len(s)
    n = len(t)
    # Let D[i,j] be the edit distance between the
    # prefix of length i of s and prefix of length j of t.
    # Then D[0,j]=j for all j and D[i,0]=i for all i.
    D = np.zeros((m+1, n+1), dtype=np.uint32)  # 0..m x 0..n
    # in earlier versions of numba, D must be allocated
    # outside of the njitted function and given as a
    # parameter! In any case, array allocation must be
    # done before the loops start.
    for j in range(n+1):
        D[0,j] = j
    for i in range(1, m+1):
        D[i,0] = i
        for j in range(1, n+1):
            neq = s[i-1] != t[j-1]
            D[i,j] = min(D[i-1,j-1]+neq, D[i-1,j]+1, D[i,j-1]+1)
    # now D[m,n] is the edit distance of the full strings
    return D[m,n]


def ed_random_strings(m, n, count=1, sigma=4):
    """
    generate pairs of random uint8 arrays,
    compute their edit distances.
    Return a list of the resulting edit distances.
    """
    ds = []
    for t in range(count):
        print("computation {}/{}".format(t+1, count))
        s = randint(sigma, size=m, dtype=np.uint8)
        t = randint(sigma, size=n, dtype=np.uint8)
        d = edit_distance(s,t)
        ds.append(d)
    return ds


def main():
    m = n = 10000
    sigma = 4
    count = 10
    ds = ed_random_strings(m, n, count=count, sigma=sigma)
    mu = np.mean(ds)
    std = np.std(ds)
    print("Edit distance between random strings of lengths {} and {}\n"
          "over an alphabet of size {} is: {:.1f} +- {:.1f}".format(
            m, n, sigma, mu, std))


if __name__ == "__main__":
    main()
