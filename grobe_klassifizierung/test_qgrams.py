from qgrams import get_canons_iterator, comp_base4, base4

__author__ = "Steffen Enders"


def test_gattaca():
    # GAT                    ATC
    # 2*16 + 0*4 + 3*1 = 35  0*16 + 3*4 + 1*1 = 13
    # ATT                    AAT
    # 0*16 + 3*4 + 3*1 = 15  0*16 + 0*4 + 3*1 = 3
    # TTA                    TAA
    # 3*16 + 3*4 + 0*1 = 60  3*16 + 0*4 + 0*1 = 48
    # TAC -> GTA
    # ...
    # ACA -> TGT
    # ...
    canons_iterator = get_canons_iterator(q_gram_size=3)
    assert list(canons_iterator([ord(a) for a in "GATTACA"])) == [13, 3, 48, 44, 4]


def test_comp():
    assert comp_base4(ord('a')) == base4(ord('t'))
    assert comp_base4(ord('A')) == base4(ord('t'))
    assert comp_base4(ord('t')) == base4(ord('a'))
    assert comp_base4(ord('T')) == base4(ord('a'))
    assert comp_base4(ord('c')) == base4(ord('g'))
    assert comp_base4(ord('C')) == base4(ord('g'))
    assert comp_base4(ord('g')) == base4(ord('c'))
    assert comp_base4(ord('G')) == base4(ord('c'))


def test_base4():
    assert base4(ord('a')) == 0
    assert base4(ord('A')) == 0
    assert base4(ord('t')) == 3
    assert base4(ord('T')) == 3
    assert base4(ord('c')) == 1
    assert base4(ord('C')) == 1
    assert base4(ord('g')) == 2
    assert base4(ord('G')) == 2