# Werkzeug zur groben Klassifizierung von Reads
Das Tool lässt sich mit `python run.py` ausführen. Weitere benötigte Parameter werden durch die Hilfe des Programms angegeben.

Eine bereits vorbereitete Datenbank mit Q-Grammen der Länge 31 ist liegt in der Datei `hla_gen_q31.fasta`.
Diese kann entsprechend für das `tablefile`-Argument übergeben werden.

Beim `classify`-Modus ist es wichtig zu beachten, dass bei der Verwendung mehrerer Threads nur die Ausgabe im `fasta`-Format möglich ist.
Falls die Ausgabe in anderen Formaten nötig ist, kann der Single-Thread Modus durch hinzufügen der Option `-m 1` aktiviert werden.
Das Dateiformat wird am Namen der Ausgabedatei erkannt.