import numba
import numpy as np

COMP_BASE4_HELP = np.array([3, 2, 0, 1])
BASE4_HELP = np.array([0, 1, 3, 2])
BASE4_MASK = 0b110


@numba.njit(cache=True)
def comp_base4(char):
    """
    Optimized as decribed in base4
    Notice: Previously, this returned the complement character for a given input.
    It will now return the qgram code of the complement instead.

    Author: Johannes May
    """
    return COMP_BASE4_HELP[(char & BASE4_MASK) >> 1]


@numba.njit(cache=True)
def base4(char):
    """
    About the optimization:
    The distance between lower and upper case ascii chars is always 32.
    Therefore the lowest 5 bit of each upper and lower case char must be the same.
    When you write the binary codes of these bits in a table, you get this:

    a/A: 00001
    t/T: 10100
    c/C: 00011
    g/G: 00111

    One can observe that the pair of the second and third bit from the right is different for all lines.
    If we apply the binary mask 110 to all of these codes and shift the result one bit to the right, the following result occurs:

    a/A: 00 -> 0
    t/T: 10 -> 2
    c/C: 01 -> 1
    g/G: 11 -> 3

    This can be used as an index for an array containing the needed qgram codes.
    The resulting code should take only 3 operations to calculate (Applying the mask, shifting the bits, looking up the result).
    We could also try to only apply the mask and then use 4 if/elif branches, which might be faster because no array lookup is needed
    
    Author: Johannes May
    """
    return BASE4_HELP[(char & BASE4_MASK) >> 1]


def get_canons_iterator(q_gram_size=31):
    """
    Returns the canons-generator for a fixed q-gram size.
    
    Author: Steffen Enders and Christoph Meyer
    """
    BITMASK = ((1 << q_gram_size) << q_gram_size) - 1
    CONSTANT = (q_gram_size << 1) - 2

    @numba.njit(cache=True)
    def update_base4_normal(base4_normal, new_byte):
        """
        Update the base4-encoding of the previous qgrams with the most recent byte
        of the sequence to match the base4-encoding of the current qgram.

        This is done by:
        *getting rid of the most significant byte of the old encoding
            -> shift the old encoding two bits to the left and use bitewise-and with the constant BITMASK
        *adding up the base4 encoding of the new byte.
            -> use bitwise or for a better performance

        Author: Steffen Enders and Christoph Meyer
        """
        return ((base4_normal << 2) & BITMASK) | base4(new_byte)

    @numba.njit(cache=True)
    def update_base4_revcomp(base4_revcomp, new_byte):
        """
        Update the base4-encoding of the current qgrams reverse complement with the most recent byte
        of the sequence to match the base4-encoding of the current qgrams reverse complement.

        This is done by:
        *getting rid of the least significant byte of the old encoding
            -> shift the old encoding two bits to the right
        *adding up the base4 encoding of the new byte as most significant byte.
            -> shift the base4 encoding of the new byte to the left and use bitewise or to add it up
        
        Author: Steffen Enders and Christoph Meyer
        """
        return (base4_revcomp >> 2) | (comp_base4(new_byte) << CONSTANT)

    @numba.njit(cache=True)
    def canons(sequence):
        """
        Yields all canonical codes of the given sequence for a fixed q-gram size.

        The function works as follows:
        We start with the values for the base4 encodings of the current qgram and
        its reverse complement both set to 0.
        Then we process the first (<q_gram_size> - 1) bytes and update the values for
        the base4-encodings for each byte.
        Starting with the <q_gram_size>-th byte, we can yield the minimum of the base4-encodings
        each time they were updated, as we have valid qgrams and valid values.
        
        Author: Steffen Enders and Christoph Meyer
        """

        # variables for the base4-encoding of the current qgram and its reverse complement
        base4_normal = 0
        base4_revcomp = 0

        # Process the first (<q_gram_size> - 1) bytes without yielding any values.
        # Note: We don't yield any values because we don't have qgrams of the fixed size.
        #       In the first iteration the q-gram has size 1, in the second size 2 (...)
        #       The steps are needed though for the first q-gram to have the correct canonical code
        for byte in sequence[:q_gram_size-1]:
            base4_normal = update_base4_normal(base4_normal, byte)
            base4_revcomp = update_base4_revcomp(base4_revcomp, byte)

        # Compute the canonical codes for all q-grams in the sequence.
        # We do this by iterating over each byte of the sequence and updating the values of
        # base4_normal and base4_revcomp for each byte.
        for byte in sequence[q_gram_size-1:]:
            base4_normal = update_base4_normal(base4_normal, byte)
            base4_revcomp = update_base4_revcomp(base4_revcomp, byte)
            # yield the minimum of the base4 encoding of the qgrams and its reverse complement
            # and thus the canonical code of the current qgram
            yield min(base4_normal, base4_revcomp)

    # return the compiled canons-function with the q-gram-size fixed to the value
    # that was specified as a parameter given to the get_canons_iterator function
    return canons
