import argparse
import os
import dinopy
import classify as classify_module
import classify_parallel as classify_p_module
from tqdm import tqdm

import logging

__author__ = "Robin Rieger"


def setup(log, parser, args):
    print("{}".format(args.hlafile))
    if any(args.hlafile.endswith(e) for e in ['fasta', 'fasta.gz', 'fa', 'fa.gz']):
        hlafile = dinopy.FastaReader(args.hlafile)
    elif any(args.hlafile.endswith(e) for e in ['fastq', 'fastq.gz']):
        hlafile = dinopy.FastqReader(args.hlafile)
    else:
        log.error("No format in HLA-File detected!")
        return

    log.debug("Tablefile: {}".format(args.tablefile))
    classify_module.generate_db(args.q, hlafile, args.tablefile)


def classify(log, parser, args):
    tablefile = args.tablefile

    if any(args.classifyfile.endswith(e) for e in ['fasta', 'fasta.gz', 'fa', 'fa.gz']):
        classifyfile = dinopy.FastaReader(args.classifyfile)

    elif any(args.classifyfile.endswith(e) for e in ['fastq', 'fastq.gz']):
        classifyfile = dinopy.FastqReader(args.classifyfile)

    else:
        log.error("No format in Classify-File detected!")
        return

    if any(args.genfile.endswith(e) for e in ['fasta', 'fasta.gz', 'fa', 'fa.gz']):
        genfile_function = dinopy.FastaWriter

        def writer_function(writer, entry, dtype=bytes):
            writer.write_entry(entry, dtype=dtype)

    elif any(args.genfile.endswith(e) for e in ['fastq', 'fastq.gz']):
        genfile_function = dinopy.FastqWriter

        def writer_function(writer, entry, dtype=bytes):
            writer.write(*entry, dtype=dtype)

    else:
        log.error("No format in Genfile detected!")
        return

    log.debug("Tablefile: {}".format(tablefile))
    log.debug("Classifyfile: {}".format(tablefile))
    log.debug("Genfile: {}".format(tablefile))

    if args.m == 1:
        with genfile_function(args.genfile) as genfile:
            classify_module.classify(args.tablefile, classifyfile, writer_function.__get__(genfile), threshold=args.t)
    else:
        classify_p_module.classify_parallel(args.tablefile, classifyfile, args.genfile, threads=args.m)


def default(log, parser, args):
    parser.print_help()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Filters HLA-Genes')
    parser.add_argument('-v', '--verbose', action='store_true', default=False)
    parser.add_argument("--version", action="version", version="%(prog)s 0.1")

    # create the top-level parser
    subparsers = parser.add_subparsers(help='sub-command help')

    # create the parser for the "setup" command
    parser_setup = subparsers.add_parser('setup', help='Creates the hashing table')
    parser_setup.add_argument('hlafile', type=str, help='Path to file with hla genes')
    parser_setup.add_argument('tablefile', type=str, help='Path to file where the hashing table is saved into')
    parser_setup.add_argument('-q', default=31, type=int, help='q for calculating the q-grams')
    parser_setup.set_defaults(func=setup)

    # create the parser for the "classify" command
    parser_classify = subparsers.add_parser('classify', help='Classifies the reads')
    parser_classify.add_argument('tablefile', type=str, help='Path to file with hla genes')
    parser_classify.add_argument('classifyfile', type=str, help='Path to file with data to classify')
    parser_classify.add_argument('genfile', type=str, help='Path to file where the hla-genes are saved into')
    parser_classify.add_argument('-t', type=int, default=2, help='The number of matching qgrams for a read to match a hla')
    parser_classify.add_argument('-m', type=int, default=3, help='Number of threads for multi-threading')
    parser_classify.set_defaults(func=classify)

    parser.set_defaults(func=default)

    args = parser.parse_args()

    logging_format = "%(asctime)-15s: %(levelname)-8s: %(name)-25s: %(message)s"
    logging.basicConfig(format=logging_format, level=(logging.DEBUG if args.verbose else logging.INFO))

    log = logging.getLogger("HLA-Filter")

    log.debug("Debug-Messages enabled")
    log.debug("Args: {}".format(args))

    args.func(log, parser, args)