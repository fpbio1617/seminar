from tqdm import tqdm
import datastructure as d
import qgrams

__author__ = "Christoph Meyer and Johannes May"


def generate_db(q_gram_size, reader, db_filename):
    """
    Generate all q-grams with the given size for all sequences in the given file
    """
    d.set_q(q_gram_size)
    canons_iterator = qgrams.get_canons_iterator(q_gram_size)
    # True is actually a keyword argument, but it has different names for fasta_reader and fastq_reader
    for read in tqdm(reader.reads(True, dtype=bytes), mininterval=1, unit="reads"):
        for ccode in canons_iterator(read.sequence):
            d.insert(ccode)

    d.serialize(db_filename)


def classify(table_filename, input_reader, writefn, threshold=2):
    """
    Classify a sequence of reads read from input_reader.
    input_reader must be a FastaReader or a FastqReader.
    writefn must be a callable accepting a tuple containing a read and a dtype argument.
    (Compatible to FastaWriter.write_entry())
    """
    d.deserialize(table_filename)
    canons_iterator = qgrams.get_canons_iterator(d.get_q())
    # True is actually the value for a keyword argument, but it has different names for fasta_reader and fastq_reader
    for read in tqdm(input_reader.reads(True, dtype=bytes), mininterval=1, unit="reads"):
        hits = 0
        for ccode in canons_iterator(read.sequence):
            if d.check(ccode):
                hits += 1
                if hits > threshold:
                    writefn(read, dtype=bytes)
                    break
