import numpy as np
import h5py

__author__ = "Daniel Hilse, Erik Rieping and Johannes May"
"""
    Wrapper for the datastructure
    The currently used datastructure is set/frozenset.
"""

dataset = set()
q_gram_size = 17


def insert(element):
    """
    Inserts new element into the datastructure.
    """

    dataset.add(element)


def check(element):
    """
    Returns true if the elemet is in the datastructure.
    """

    return element in dataset


def count():
    """
    Returns the Number of elements in the datastructure.
    """

    return len(dataset)


def set_q(q):
    """
    Setter for q_gram_size
    """

    global q_gram_size
    q_gram_size = q


def get_q():
    """
    Getter for q_gram_size
    """

    # global q_gram_size
    return q_gram_size


def serialize(filename):
    """
    Serializes the datastructure to hdf5-file
    Parameter filename: filename for output file.
    """

    nparray = np.array(list(dataset))

    with h5py.File(filename, "w") as f:
        f.create_dataset("q", data=q_gram_size)
        f.create_dataset("ccodes", data=nparray)


def deserialize(filename):
    """
    Deserializes the datastructure from given hdf5-file.
    Parameter filename: filename for input file.
    """

    global dataset
    global q_gram_size

    with h5py.File(filename, "r") as f:
        dataset = frozenset(f["ccodes"][:])
        q_gram_size = f["q"][()]
    return False
