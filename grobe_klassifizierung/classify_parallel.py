from tqdm import tqdm
import datastructure as d
import qgrams
import sys
import os
from multiprocessing import Pool
from threading import Semaphore
import dinopy
import time

__author__ = "Daniel Hilse, Erik Rieping"

POOL_SIZE = 3
CHUNK_SIZE = 1000000
THRESHOLD = 2


def do_classify(reads, names, output_filename, chunk, threshold):
    """
    Classify one chunk and write positive-classified reads to temporary output file.
    Parameters:
        reads: list of reads sequence (as bytes)
        names: list of reads names (string)
        output_filename: output_filename
        chunk: chunk number
        threshold: threshold for positive classification
    """
    canons_iterator = qgrams.get_canons_iterator(d.get_q())
    index = 0
    with dinopy.FastaWriter(get_tmp_file(output_filename, chunk), force_overwrite=True) as writer:
        for i, read in enumerate(reads):
            hits = 0
            for ccode in canons_iterator(read):
                if d.check(ccode):
                    hits += 1
                    if hits > threshold:
                        writer.write_entry((read, names[index]), dtype=bytes)
                        break
            index += 1

    return chunk


def classify_parallel(table_filename, input_reader, output_filename, read_chunk_size=CHUNK_SIZE, threshold=THRESHOLD, threads=POOL_SIZE):
    """
    Iterates input_reader and starts parallel computation on worker processes,
    writes output file.
    
    Parameters:
        table_filename: in "setup" generated database file
        input_reader: fasta/fastq reader instance for intput file
        output_filename: output filename
        read_chunk_size: chunk size for computation in one worker process, default 1000000
        threshold: threshold for positive classification, default 2
        threads: number of worker processes, default 3
    """

    d.deserialize(table_filename)

    semaphore = Semaphore(threads)

    # check for tmp directory
    if not os.path.exists("tmp"):
        os.makedirs("tmp")

    def work_done(arg):
        semaphore.release()

    with Pool(threads, maxtasksperchild=2) as p:

        read_cache = list()
        name_cache = list()
        current_chunk = 0
        index = 0

        for read in tqdm(input_reader.reads(True, dtype=bytes), mininterval=1, unit=" reads"):
            if index >= read_chunk_size:
                # pass chunk to process pool

                # using semaphore because apply_async doesn't block on full queue, wich leads to memory issues
                semaphore.acquire()
                p.apply_async(do_classify, args=(read_cache, name_cache, output_filename, current_chunk, threshold), callback=work_done)
                time.sleep(1)
                current_chunk += 1
                read_cache = list()
                name_cache = list()
                index = 0

            read_cache.append(read.sequence)
            name_cache.append(read.name)
            index += 1

        # pass last chunk
        if (index > 0) or (current_chunk == 0):

            p.apply(do_classify, args=(read_cache, name_cache, output_filename, current_chunk, threshold))
            current_chunk += 1

        # wait for worker processes to finish
        p.close()
        p.join()

        print("Merging output file")
        merge_output_file(output_filename, current_chunk, remove_tmp=True)


def merge_output_file(output_filename, num_chunks, remove_tmp=True):
    """
    Merges temporary files to one output_file.
    Optionally removes the temporary files.
    """

    with open(output_filename, "w") as outfile:
        for i in range(num_chunks):
            with open(get_tmp_file(output_filename, i)) as infile:
                for line in infile:
                    outfile.write(line)

    if(remove_tmp):
        for i in range(num_chunks):
            os.unlink(get_tmp_file(output_filename, i))


def get_tmp_file(output_filename, chunk):
    """
    Returns temporary filename for given chunk.
    """

    return "tmp/" + os.path.split(output_filename)[1] + "_chunk_" + str(chunk) + ".fasta"
