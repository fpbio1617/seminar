import numpy as np
import pandas as pd

# Dataframe aus einer HDF5-Datei laden
inf = pd.read_hdf("influenza.h5")
print(inf.head())

# Gruppieren der Einträge nach QGrammen
grouped = inf.groupby('QGrams')

# Mittelwerte der übrigen Spalten im gruppierten Dataframe
grouped.mean().head()
print(grouped.mean().head())

# Mehrere Aggregats-Funktionen anwenden
grouped.aggregate([max, min, np.mean])
print(grouped.aggregate([max, min, np.mean]).head())

# Distanzen mit verschiedenen Funktionen aggregieren
dis = grouped['Distances'].agg({ 'Amount' : np.size, 'Max_Distance' : max, 'Min_Distance' : min, 'Mean_Distance' : np.mean})
print(dis.head())

# Mittelwert aller Spalten im Dataframe bilden
inf.apply(np.mean)
print(inf.apply(np.mean).head())

# Markieren der QGramme, bei denen maximale und minimale Distanz unterschiedlich sind.
dif = dis['Max_Distance'] != dis['Min_Distance']
print(dif.head())

# Dataframe in HDF5-Datei ablegen.
dif.to_hdf("difs.h5", "dif")

# Diesen Dataframe wieder einlesen.
dif_r = pd.read_hdf("difs.h5", "dif")
print(dif_r.head())