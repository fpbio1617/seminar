# Ausarbeitung
Die Datei `ausarbeitung.pdf` beinhaltet unsere schriftliche Ausarbeitung und gibt einen
groben Überblick, was mit pandas möglich ist.

# Demos
Es gibt zwei Demos, die mit `python basic_demo.py` oder `python advanced_demo.py` ausgeführt werden können.
Bei Demos enthalten kommentierten Code, man sollte sich also den Code der Demos anschauen und
gegebenfalls in der Ausgabe das Resultat bestimmter Operationen heraussuchen.

Die Ausgabe ist leider nicht besonders übersichtlich, vermutlich ist es daher hilfreich,
vorher nicht benötigte `print`-Aufrufe auszukommentieren.

Bei dem `advanced_demo.py`-Skript wird die `influenza.h5` Datei geladen,
diese beinhaltet einen Dataframe mit den Spalten `QGrams` und `Distances`,
die jeweils QGramme und die zugehörigen Distanzwerte beinhalten.