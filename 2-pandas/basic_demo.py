import pandas as pd
import numpy as np

#leeres DataFrame erzeugen
df = pd.DataFrame()
print(df)
#Spalte zu einem DataFrame hinzufügen
df["Spalte1"] = pd.Series(range(10))
print(df)

df["Spalte2"] = range(10, 0, -1)
df["😂 😉 😋"] = list("unicode123")
print(df)


#Zugriff auf einzelne Spalte
df["Spalte1"]
print(df["Spalte1"])

df.Spalte2
print(df.Spalte2)

#auch unicode möglich
df["😂 😉 😋"]
print(df["😂 😉 😋"])

#mehrere Spalten Selektieren
df[["Spalte1", "😂 😉 😋"]]
print(df[["Spalte1", "😂 😉 😋"]])

#Zeilen ausgeben
df[2:4]
print(df[2:4])

#oder auch per DateFrame.loc
df.loc[3]
print(df.loc[3])


#hinzufügen einer Zeile am Ende
df.loc[len(df)] = [9001, 9001, "😎"]
print(df)


#weitere möglichkeiten zum Erstellen von DataFrames:
df2 = pd.DataFrame(np.random.randn(10, 3), index=range(100, 110), columns=["A", "B", "C"])
print(df2)

#Operatoren wie *, +, -, /, usw. werden überladen
df2.A * 5
print(df2.A * 5)

df2.B + 100
print(df2.B + 100)

df2
print(df2)

df2.B += 100
print(df2)

#Diese Operationen sind auch zwischen mehreren Spalten möglich
df2["SUM"] = df2.A + df2.B + df2.C
print(df2)


#man kann auch Vergleiche anstellen, die eine Series von Booleans liefern:
df2.B > 100
print(df2.B>100)

#und eine Series von Booleans zum Filtern benutzen
df2[df2.B > 100]
print(df2[df2.B > 100])

df2[df2.B > 100][["A", "B"]]
print(df2[df2.B > 100][["A", "B"]])
